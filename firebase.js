import firebase from "firebase/app"
import "firebase/auth"

const app = firebase.initializeApp({
  apiKey: "AIzaSyCo-d0eHxqG260091MgssPSsCc_oOD-Vg8",
  authDomain: "login-55d66.firebaseapp.com",
  projectId: "login-55d66",
  storageBucket: "login-55d66.appspot.com",
  messagingSenderId: "30728420075",
  appId: "1:30728420075:web:7cdd6333dd8da3c4cd9cae",
  measurementId: "G-V67N0FKX1D"
})

export const auth = app.auth()
export default app
